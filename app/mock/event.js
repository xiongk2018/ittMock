var EventEmitter = require('events').EventEmitter;
var domain = require('domain');
//var logger = require('./app/log').logger;
var d = domain.create();
d.on('error',function(err){
   // logger.error(err);
});
d.run(function(){
    function EventObj(){
        this.event = new EventEmitter();
        this.event.setMaxListeners(0);
        this.funcNameObj = {};
    }
    var event = new EventObj();
    module.exports = event;
})
