var sections = require('../../datas/section').sections;
var _ = require('underscore');

//解析前端提交的基本运行图模拟数据，生成（发车时间，路径）对象数组
exports.createTramRunPlan = function( data ){
//  console.log(data);
  var downOperationPath = data.downOperationPath;
  var upOperationPath = data.upOperationPath;
  var downStartTime = getSeconds(data.downStartTime); 
  var upStartTime = getSeconds(data.upStartTime);
  var upNum = data.upNum; 
  var downNum = data.downNum;
  //发车间隔
  var interval = data.interval;
  //上行发车运行总次数,从起点到终点算一次
  var upTotal = data.upTotal;
  //下行发车运行总次数
  var downTotal = data.downTotal; 
  var obj = {};
  obj.avgSpeed = data.avgSpeed;
  obj.avgStopTime = data.avgStopTime;
  obj.returnTime = data.returnTime;
  var tramRunPlan = new Array();
  if(upNum > downNum){
    for( var i = 0; i < downNum; i++){    
        var downTime = downStartTime + i*interval*60;
        var upTime = upStartTime + i*interval*60;
        var downObj = {};
        downObj.startTime = downTime;
        downObj.path = downOperationPath;
        downObj.runTimes = downTotal;
        _.extend(downObj,obj);
        tramRunPlan.push(downObj);
        var upObj = {};
        upObj.startTime = upTime;
        upObj.path = upOperationPath;
        upObj.runTimes = upTotal;
        _.extend(upObj,obj);
        tramRunPlan.push(upObj);
        }     
   
    for(var i = 0; i < ( upNum - downNum); i++){
      var upTime = upStartTime + interval*downNum*60 + i* interval*60;
      var upObj = {};
      upObj.startTime = upTime;
      upObj.path = upOperationPath;
      upObj.runTimes = upTotal;
      _.extend(upObj,obj);
      tramRunPlan.push(upObj);    
    }
  
  }else{
     for( var i = 0; i < upNum; i++){
        var downTime = downStartTime + i*interval*60;
        var upTime = upStartTime + i*interval*60;
        var downObj = {};
        downObj.startTime = downTime;
        downObj.path = downOperationPath;
        downObj.runTimes = downTotal;
        _.extend(downObj,obj);
        tramRunPlan.push(downObj);
        var upObj = {};
        upObj.startTime = upTime;
        upObj.path = upOperationPath;
        upObj.runTimes = upTotal;
        _.extend(upObj,obj);
        tramRunPlan.push(upObj);
        }     
   
    for(var i = 0; i < ( downNum - upNum); i++){
      var downTime = downStartTime + interval*upNum*60 + i* interval*60;
      var downObj = {};
      downObj.startTime = downTime;
      downObj.path = downOperationPath;
      downObj.runTimes = downTotal;
      _.extend(downObj,obj);
      tramRunPlan.push(downObj);    
    }
   
  }
    return tramRunPlan;
}


exports.getNextSection = function(fromCode, toCode){
  for(var index in sections){
    if(sections[index].fromTerminus === toCode && sections[index].toTerminus!=fromCode){
	   return sections[index];
	}
   
  }
  return null;
}
exports.getCurrentSection = function(fromCode, toCode){
  for(var index in sections){
    if((fromCode == null || sections[index].fromTerminus === fromCode)&&(toCode == null || sections[index].toTerminus === toCode)){
	   return sections[index];
	}
  
  }
  return null;
}

exports.getSectionByCode = function( code ){
  for(var index in sections){
    if( code === sections[index].code){
	   return sections[index];
	}
  }
  return null;
}


var getSeconds =exports.getSeconds =function (time){
   if(time){
	   var hour = parseInt(time.split(':')[0]);
	   var tempMins = time.split(':')[1];
   if(tempMins.charAt(0)==='0'){
       mins = parseInt(tempMins.charAt(1));
   }else{
       mins = parseInt(tempMins);
   }
   return hour*60*60 + mins*60;
   
   } 
 
 }


exports.getSysTimeBySecs = function (){
 var date = new Date();
 if(date){
	return date.getHours()*3600 + date.getMinutes()*60 + date.getSeconds();
 }
}


