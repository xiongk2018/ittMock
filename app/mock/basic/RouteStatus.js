var _ = require("underscore");

function RouteStatus(opts){
	this.id = opts.id;
	this.code = opts.code;
	this.isLock = false;
	this.opStatus;
	this.routeItems = opts.routeItems;
	this.nearSegmentCode = opts.nearSegmentCode;
	this.leaveSegmentCode = opts.leaveSegmentCode;
	this.inIndicatorCode = opts.inIndicatorCode;
	this.outIndicatorCode = opts.outIndicatorCode;
	this.dest = opts.dest;
	this.direct = opts.direct;	
    this.type = opts.type;
    this.nextRouteId = opts.nextRouteId;
    this.startTerminus = opts.startTerminus;
	this.runExecutor;
}

module.exports = RouteStatus;

//TODO 车辆运行监测 暂时使用车辆调用监测
RouteStatus.prototype.reactToTram = function(tramStatus){
	var $this = this;
/*  if(tramStatus.currentRoute.code != $this.code){
  	return;
  }else{*/
	  _.forEach(this.routeItems, function (ele) {
		  if(tramStatus.direct === ele.actualDirect){
			  if (ele.inSegmentCode && tramStatus.currentSegment.code === ele.inSegmentCode ){
				  if(ele.type === "indicator"){
					  var indicator = $this.runExecutor.getIndicatorStatusByCode(ele.itemCode);
					  indicator.changeStatus(ele.needStatus);
				  }
				  else if( ele.type === "turnout"){
					  var turnoutStatus = $this.runExecutor.getTurnoutStatusByCode(ele.itemCode);
					  turnoutStatus.changeStatus(ele.needStatus);
					  turnoutStatus.occupy();
				  }
			  }
			  if (ele.inSegmentCode && tramStatus.outSegmentCode === ele.outSegmentCode){
				  if(ele.type === "indicator"){
					  var indicator = $this.runExecutor.getIndicatorStatusByCode(ele.itemCode);
					  indicator.free();
				  }
				  else if( ele.type === "turnout"){
					  var turnoutStatus = $this.runExecutor.getTurnoutStatusByCode(ele.itemCode);
					  turnoutStatus.free();
				  }
			  }
		  }

	  })
 // }

}
/*开放进路*/
RouteStatus.prototype.open = function(){
  var $this = this;  
  _.forEach(this.routeItems,function(ele,index){
    if(ele.type === "indicator"){
    	var indicator = $this.runExecutor.getIndicatorStatusByCode(ele.itemCode);
    	indicator.changeStatus(ele.needStatus);
    }
    else if( ele.type === "turnout"){
    	var turnoutStatus = $this.runExecutor.getTurnoutStatusByCode(ele.itemCode);
    	turnoutStatus.changeStatus(ele.needStatus);
    	turnoutStatus.open();
    }
  /*  else if(ele.type === "segment"){
    	var segmentStatus = $this.runExecutor.getSegmentStatusByCode(ele.itemCode);
    	segmentStatus.open();
    }*/
  })

}

RouteStatus.prototype.free = function(){
 var $this = this;   
 _.forEach(this.routeItems,function(ele){
 	if(ele.type === "indicator"){
       // console.log($this.runExecutor);
    	var indicator = $this.runExecutor.getIndicatorStatusByCode(ele.itemCode);
    	indicator.free();
    }
    else if( ele.type === "turnout"){
    	var turnoutStatus = $this.runExecutor.getTurnoutStatusByCode(ele.itemCode);
    	turnoutStatus.free();
    }
   /* else if(ele.type === "segment"){
    	var segmentStatus = $this.runExecutor.getSegmentStatusByCode(ele.itemCode);
    	segmentStatus.free();
    }*/
 })	
}

RouteStatus.prototype.occupy = function(){
 var $this = this;   
 _.forEach(this.routeItems,function(ele){
 	if(ele.type === "indicator"){
    	var indicator = $this.runExecutor.getIndicatorStatusByCode(ele.itemCode);
		indicator.changeStatus(ele.needStatus);
    }
    else if( ele.type === "turnout"){
    	var turnoutStatus = $this.runExecutor.getTurnoutStatusByCode(ele.itemCode);
		turnoutStatus.changeStatus(ele.needStatus);
    	turnoutStatus.occupy();
    }
  /*  else if(ele.type === "segment"){
    	var segmentStatus = $this.runExecutor.getSegmentStatusByCode(ele.itemCode);
    	segmentStatus.occupy();
    }*/
 })		
}

RouteStatus.prototype.isOccupy = function(){
	return this.opStatus === "occupy";
}