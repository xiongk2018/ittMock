'use strict';
define(function(require , exports , module){

	module.exports = function(app){
		app.register.controller("showMessageCtrl" ,function($rootScope , $scope , $timeout , App){
			$scope.logContent = "";
			$scope.command = "";
			App.addListener("message.receive" , function(e , data) {
				$scope.logContent += data + "\n";
				$scope.$apply();
			});
			
			$scope.sendCommand = function(){
				App.sendCommand("COMMAND.DEVICE" , $scope.command);
			}
		});
	}
});