 var fs = require("fs");
 var data = eval("(" + fs.readFileSync("./config/properties.json", "utf-8")+ ")");
 var CLIENT_PORT = data.udpClientPort;
 var SERVER_PORT = data.udpServerPort;
 var CLIENT_IP = data.udpClientIP;
 var SERVER_IP = data.udpServerIP;
 var bufferUtil = require('../net/buffer');
 var domain = require('domain');
 var eventObj = require('../mock/event');
 var limitSpeedObjArr = [];
 var _ = require('underscore');
 var dgram = require('dgram');
 var logger = require('../log').logger;
 var os  = require('os-utils');
 var server = dgram.createSocket('udp4'); // ipv4
 exports.limitSpeedObjArr = limitSpeedObjArr;

 eventObj.event.on('tram_status_change_event',function(datas){
 	_.forEach(datas,function(ele){
		//logger.warn(ele);
 		var buffer = bufferUtil.createTramStatus(ele);
 		sendInfo(buffer);
 	});
   
  });
 eventObj.event.on('tram_register_send_event',function(data){
     var buffer = bufferUtil.createTramRegister(data);
	 sendInfo(buffer);
   });
 eventObj.event.on('tram_runPlanArgs_send_event',function(data){
	 var buffer = bufferUtil.createRunArgs(data);
	 sendInfo(buffer);
 })
 eventObj.event.on("tram_packetReply_send_event",function(data){
	 var buffer = bufferUtil.createTramReply(data);
	 sendInfo(buffer);
 })

 eventObj.event.on("tram_gps_change_event",function(data){
	 var buffer = bufferUtil.createGPSInfo(data);
	 sendInfo(buffer);
 })
function sendInfo(buffer){
   server.send(buffer, 0, buffer.length, CLIENT_PORT, CLIENT_IP, function(err, bytes) {		   
		if (err) throw err;
		//client.close();				
	 })
}

 exports.createUdpServer = function (){
   // var server = dgram.createSocket('udp4');
	server.on('listening', function () { 	
	  var address = server.address();  
	  logger.info('UDP Server listening on ' + address.address + ":" + address.port);
		logger.warn('UDP Server listening on ' + address.address + ":" + address.port);
	 });
	server.on('message', function (message, remote) {
		//console.log(message);
		var bufArr = bufferUtil.splitBuffer(message);
		for(var i = 0; i < bufArr.length; i++){
			var mes = bufArr[i];
		//	console.log(mes);
			var typeCode = bufferUtil.getDataType( mes )
		//	console.log("******"+typeCode);
			if(typeCode == 0x24){
				var tramText = bufferUtil.getTextMsg( mes );
				eventObj.event.emit('tram_text_reply_event',tramText );
			} else if( typeCode == 0x52){
				var replyPacket = bufferUtil.getReplyPacket( mes );
				//logger.info('get reply from center');
				eventObj.event.emit('tram_register_reply_event',replyPacket);
			}else if(typeCode === 0x16){
				var runArgsInfo = bufferUtil.getOperationPlan(mes);
				//console.log(runArgsInfo);
				eventObj.event.emit('tram_runArgs_reply_event',runArgsInfo);
			}else if(typeCode === 0x18){
				var scheduleInfo = bufferUtil.getScheduleInfo(mes);
				eventObj.event.emit('tram_schedule_reply_event',scheduleInfo);
			}else if( typeCode == 0x22){
				var limitSpeedObj = bufferUtil.getLimitSpeedObj( mes );
				//logger.info(limitSpeedObjArr);
			}else if(typeCode == 0x64){
				var centerReplyObj = bufferUtil.getCenterReply(mes);
				//console.log(centerReplyObj);
				eventObj.event.emit('tram_packet_reply_event',centerReplyObj);
			} else{
				console.log("unKnown type code");
			}
		}

	  });
	server.bind(SERVER_PORT, SERVER_IP);
}
 function createLimitSpeedObjArr( limitSpeedObj ){
    if(limitSpeedObjArr.length!=0){
	  for(var i in limitSpeedObjArr){
	    if(limitSpeedObj && limitSpeedObj.tramCode == limitSpeedObjArr[i].tramCode){
		  limitSpeedObjArr.remove(i);
		  limitSpeedObjArr.push(limitSpeedObj);
		  return;
		}
	  }
	 limitSpeedObjArr.push(limitSpeedObj);  
	}else{
	 limitSpeedObjArr.push(limitSpeedObj);  
	}
   
 }

