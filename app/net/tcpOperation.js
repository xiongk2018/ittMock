var net = require('net');
var eventObj = require('../mock/event');
var bufferUtil = require('../net/hkBuffer');

var HOST = '192.168.0.6';
var PORT = 4300;

eventObj.event.on('hk_event_mock',function(data){
    console.log(data);
    //var hkBuffer = bufferUtil.createBuffer(data);
    sendHkBuffer(data);
})

function sendHkBuffer(buffer){
    var client = new net.Socket();
    client.connect(PORT, HOST, function() {
        console.log('CONNECTED TO: ' + HOST + ':' + PORT);
        // 建立连接后立即向服务器发送数据，服务器将收到这些数据
        client.write(buffer);
    });
// 为客户端添加“data”事件处理函数
//data是服务器发回的数据
    client.on('data', function(data) {
        //console.log( data);
        bufferUtil.resolveStr(data.toString());
        //eventObj.event.emit("hk_event_c_command",bufferUtil.resolveBuffer(data));
        // 完全关闭连接
       // client.destroy();
    });
// 为客户端添加“close”事件处理函数
    client.on('close', function() {
        console.log('Connection closed');
    });
}
// 创建一个TCP服务器实例，调用listen函数开始监听指定端口
// 传入net.createServer()的回调函数将作为”connection“事件的处理函数
// 在每一个“connection”事件中，该回调函数接收到的socket对象是唯一的
net.createServer(function(sock) {
    // 我们获得一个连接 - 该连接自动关联一个socket对象
    console.log('CONNECTED: ' +
    sock.remoteAddress + ':' + sock.remotePort);

    // 为这个socket实例添加一个"data"事件处理函数
    sock.on('data', function(data) {
        console.log( data);
        // 回发该数据，客户端将收到来自服务端的数据
        //  sock.write('You said "' + data + '"');
    });

}).listen(6969, '127.0.0.1');
