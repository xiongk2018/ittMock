/**************************************************************************/
/** 
 * Map对象，实现Map功能 
 *  
 *  
 * size() 获取Map元素个数  
 * isEmpty() 判断Map是否为空  
 * clear() 删除Map所有元素  
 * put(key, value) 向Map中增加元素（key, value)  
 * remove(key) 删除指定key的元素，成功返回true，失败返回false  
 * get(key) 获取指定key的元素值value，失败返回null  
 * element(index) 获取指定索引的元素（使用element.key，element.value获取key和value），失败返回null  
 * containsKey(key) 判断Map中是否含有指定key的元素  
 * containsValue(value) 判断Map中是否含有指定value的元素  
 * keys() 获取Map中所有key的数组（array）  
 * values() 获取Map中所有value的数组（array） 
 */  
function Map() {  
    this.elements = new Array();  
  
    // 获取Map元素个数  
    this.size = function() {  
        return this.elements.length;  
    },  
  
    // 判断Map是否为空  
    this.isEmpty = function() {  
        return (this.elements.length < 1);  
    },  
  
    // 删除Map所有元素  
    this.clear = function() {  
        this.elements = new Array();  
    },  
  
    // 向Map中增加元素（key, value)  
    this.put = function(_key, _value) {  
        if (this.containsKey(_key) == true) {  
            if (this.containsValue(_value)) {  
                if (this.remove(_key) == true) {  
                    this.elements.push( {  
                        key : _key,  
                        value : _value  
                    });  
                }  
            } else {  
                this.elements.push( {  
                    key : _key,  
                    value : _value  
                });  
            }  
        } else {  
            this.elements.push( {  
                key : _key,  
                value : _value  
            });  
        }  
    },  
  
    // 删除指定key的元素，成功返回true，失败返回false  
    this.remove = function(_key) {  
        var bln = false;  
        try {  
            for (i = 0; i < this.elements.length; i++) {  
                if (this.elements[i].key == _key) {  
                    this.elements.splice(i, 1);  
                    return true;  
                }  
            }  
        } catch (e) {  
            bln = false;  
        }  
        return bln;  
    },  
  
    // 获取指定key的元素值value，失败返回null  
    this.get = function(_key) {  
        try {  
            for (i = 0; i < this.elements.length; i++) {  
                if (this.elements[i].key == _key) {  
                    return this.elements[i].value;  
                }  
            }  
        } catch (e) {  
            return null;  
        }  
    },  
  
    // 获取指定索引的元素（使用element.key，element.value获取key和value），失败返回null  
    this.element = function(_index) {  
        if (_index < 0 || _index >= this.elements.length) {  
            return null;  
        }  
        return this.elements[_index];  
    },  
  
    // 判断Map中是否含有指定key的元素  
    this.containsKey = function(_key) {  
        var bln = false;  
        try {  
            for (i = 0; i < this.elements.length; i++) {  
                if (this.elements[i].key == _key) {  
                    bln = true;  
                }  
            }  
        } catch (e) {  
            bln = false;  
        }  
        return bln;  
    },  
  
    // 判断Map中是否含有指定value的元素  
    this.containsValue = function(_value) {  
        var bln = false;  
        try {  
            for (i = 0; i < this.elements.length; i++) {  
                if (this.elements[i].value == _value) {  
                    bln = true;  
                }  
            }  
        } catch (e) {  
            bln = false;  
        }  
        return bln;  
    },  
  
    // 获取Map中所有key的数组（array）  
    this.keys = function() {  
        var arr = new Array();  
        for (i = 0; i < this.elements.length; i++) {  
            arr.push(this.elements[i].key);  
        }  
        return arr;  
    },  
  
    // 获取Map中所有value的数组（array）  
    this.values = function() {  
        var arr = new Array();  
        for (i = 0; i < this.elements.length; i++) {  
            arr.push(this.elements[i].value);  
        }  
        return arr;  
    };  
} 

/** 转换平行数据为树结构 **/
var simpleData2Tree = function(datas , key , parentKey , childKey) {
	var i,l;
	if (!key || key=="" || !datas) return [];
	var r = [];
	var tmpMap = [];
	for (i=0, l=datas.length; i<l; i++) {
		tmpMap[datas[i][key]] = datas[i];
	}
	for (i=0, l=datas.length; i<l; i++) {
		if (tmpMap[datas[i][parentKey]] && datas[i][key] != datas[i][parentKey]) {
			if (!tmpMap[datas[i][parentKey]][childKey])
				tmpMap[datas[i][parentKey]][childKey] = [];
			tmpMap[datas[i][parentKey]][childKey].push(datas[i]);
		} else {
			r.push(datas[i]);
		}
	}
	return r;
};
/**
 * 简单log输出控制。
 * 参数；flag：可以作为全局控制参数。
 * by Leon.Wang 2012年11月28日10:34:28
 */
var flagAll = true;
var log4js = function(flag, msg){
    if(flag){
        if(window.console){
            console.log( typeof msg === "object" ? JSON.stringify(msg) : msg );
        }else{
            alert( typeof msg === "object" ? JSON.stringify(msg) : msg );
        }
    }
};

/**
 * 数组删除
 */
Array.prototype.remove=function(index){
  if(isNaN(index) || index > this.length){
	return false;
  }
  
  for(var i=0,n=0;i<this.length;i++){
	if(this[i]!= this[index]){
		this[n++]=this[i];
	  }
  }
  this.length -= 1;
};

/**
 * 数组排序
 *//*
Array.prototype.sort = function(){
	for(var x=0;x<this.length;x++){
        for(var y=x+1;y<this.length;y++){
        	if(this[x]>this[y]){
                var temp=this[x];
                this[x]=this[y];
                this[y]=temp;        
            }
        }
	}
	return this;
}*/

var Compare ={
				BEFORE : "BEFORE",
				AFTER : "AFTER",
				EQUAL : "EQUAL"
			};	

Date.prototype.compare = function(another){
	if(this.getFullYear()-another.getFullYear()<0)
		return Compare.BEFORE;
	else if(this.getFullYear()-another.getFullYear()>0)
		return Compare.AFTER;
	else{
		if(this.getMonth()-another.getMonth()<0)
			return Compare.BEFORE;
		else if(this.getMonth()-another.getMonth()>0)
			return Compare.AFTER;
		else{
			if(this.getDate()-another.getDate()<0)
				return Compare.BEFORE;
			else if(this.getDate()-another.getDate()>0)
				return Compare.AFTER;
			else
				return Compare.EQUAL;
		}
	}
};



//禁用鼠标右键
//document.oncontextmenu = function(){return false};
//禁用F5
document.onkeydown = function() {
    if (event.keyCode == 116)          
    {          
        event.keyCode = 0;          
        event.cancelBubble = true;          
        return   false;          
    }
};              

//替换所有要替换的文字
String.prototype.replaceAll = function (originStr,destStr){  
	return this.replace(eval("/"+originStr+"/gi"),destStr);  	
}