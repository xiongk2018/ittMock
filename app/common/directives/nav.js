'use strict';
var eventObj = require("./app/mock/event");

define(function(require , exports){
	require("store");
	var tpl = require("../tpl/nav.html");
	angular.module("headerNav" , ["ngRoute" , "App"])
	.factory("operationPathService",function($http,$filter){
		var getOperationPaths = function(){
				var downOperationPaths = new Array();
				var upOperationPaths = new Array();
				var operationPaths = new Array();			
		       $http({
				    method:"GET",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
				    url:"datas/operationPath.json"
			     }) .then(function(res){			   
			    var data = res.data;
				for ( var i = 0 , len = data.length ; i < len ; i++ ){
						
					if ( data[i].type == "down" ){ 
						downOperationPaths.push(data[i]);
					}
					else{
						upOperationPaths.push(data[i]);
					}	
					operationPaths.push(data[i]); //新增时的路线选择
				}
			});
		 return operationPaths;     
		}

		return {
			operationPaths : getOperationPaths()
		}
	})
    .filter("stationName" , function(App){
       return function(text){
       	 var backValue = null;
       	 _.forEach(App.Terminus,function(ele){
       	 	if(ele.code === text){
       	 		_.forEach(App.Stations,function(e){
       	 			if (ele.stationId === e.id) {
       	 				backValue = e.name;
       	 			}
       	 		})
       	 	}
       	 })      
       	return backValue;      	 
       }
    })
	.controller('headerNavCtrl' , function($scope , App , $dialogs, operationPathService){
		$scope.addTram = function() {
			var dlg = $dialogs.create('./app/monitor/tpl/addTram.html',function($scope , $modalInstance , App){
                $scope.tramAddPlan = {};
                $scope.tramAddPlan.avgSpeed = 72;
                $scope.tramAddPlan.avgStopTime = 30;
                $scope.tramAddPlan.startTime = "9:00";
                $scope.tramAddPlan.reentryTime = 15;
                $scope.tramAddPlan.runTimes = 2;
				$scope.cancel = function(){
					$modalInstance.dismiss('canceled');
				};
				$scope.add = function() {
					eventObj.event.emit("app_event_tram_add", $scope.tramAddPlan);
					this.cancel();
				};
				$scope.operationPaths = operationPathService.operationPaths;
				$scope.terminuses = [];
				$scope.$watch('tramAddPlan.path',function(newValue,oldValue){
					if (newValue === oldValue) {
					   return;
					}else{
						var direct = null;
						_.forEach($scope.operationPaths,function(ele){
							if(ele.path === $scope.tramAddPlan.path){
                               direct = ele.type;
							}
						})
                       $scope.terminuses = _.filter(App.Terminus,function(ele){                      	
                   	     return ele.direct === direct && ele.property != "virtual";
                       })
                       //当不同方向时，将车站顺序排序
                       if(direct === "up"){
                          $scope.terminuses = _.sortBy($scope.terminuses,function(ele){
                          	return -Number(ele.code);
                          })
                       }
						//去掉最后一个站
						$scope.terminuses = $scope.terminuses.slice(0,$scope.terminuses.length-1);
                       
					}
				})
				App.changeModalWidth(1500);
			},{},{key: false,back: 'static'});			
		}
		$scope.setSimulate = function(){
			var dlg = $dialogs.create('./app/monitor/tpl/createMonitor.html',function($scope , $modalInstance , App){
				$scope.basePlan = {};
				$scope.basePlan.downOperationPath = "227-225-223-221-219-217-215-213-211";
				$scope.basePlan.upOperationPath = "211-213-215-217-219-221-223-225-227";		
				$scope.basePlan.interval = 15;
				$scope.basePlan.avgSpeed = 30;
				$scope.basePlan.downNum = 0;
				$scope.basePlan.upNum = 1;
				$scope.basePlan.returnTime = 15;
				$scope.basePlan.avgStopTime = 30;							
				$scope.basePlan.downTotal = 2;
				$scope.basePlan.upTotal = 2;						
				$scope.basePlan.downStartTime = "9:00";
				$scope.basePlan.upStartTime = "9:00";
				$scope.tram = {
					serviceNumType:0
				};
				//$scope.basePlan.name = "angular";
				$scope.cancel = function(){
					$modalInstance.dismiss('canceled');;
				};
				$scope.dataInput = function() {
					eventObj.event.emit("app_event_tram_simulate", $scope.basePlan);
					console.log($scope.basePlan.name);
					this.cancel();
				}
				$scope.uploadTest = function(){
                   console.log($scope.name);
				}
				App.changeModalWidth(1500);
			},{},{key: false,back: 'static'});
		}
		$scope.startDTMock = function(){
			 eventObj.event.emit("app_event_dt_simulate","ok");
		}
		$scope.setArgs = function () {
			var dlg = $dialogs.create('./app/monitor/tpl/setArgs.html',function($scope,App,$modalInstance,$http){
				$scope.netArgs = {};
				$http({
					method:"GET",
					url:"config/properties.json"
				}).then(function(res){
					$scope.netArgs.udpClientIP = res.data.udpClientIP;
					$scope.netArgs.udpClientPort = res.data.udpClientPort;
					$scope.netArgs.udpServerIP = res.data.udpServerIP;
					$scope.netArgs.udpServerPort = res.data.udpServerPort;
				})
				$scope.saveArgs = function () {
                  eventObj.event.emit("app_event_netArgs_update",$scope.netArgs);
					this.cancel();
				}

				$scope.cancel = function(){
					$modalInstance.dismiss('canceled');
				};
			},{},{key:false,back:'static'});
		}
		

	})
	.directive('headerNav' , ["$location" , function($location){
		return {
			restrict: 'A',
			replace: true,
			template: tpl,
			controller: "headerNavCtrl",
			link:function($scope , $element , $attr){}
		}
	}]);
});