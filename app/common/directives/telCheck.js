define(function(require , exports , module){
	angular.module("ngTel" , [])
	.directive('ngTel' , ['$http',function($http){
		return {
			require: 'ngModel',
			link:function($scope ,  $element , $attrs , $ctrl){
				$element.on('blur', function (evt) {
					var value = $scope.$eval($attrs.ngModel);
					if(value!=null && value.length!=0){
						var check = /(^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$)|(^0{0,1}1[3|4|5|6|7|8|9][0-9]{9}$)/.test(value); 
						$scope.$apply(function(){
							 $ctrl.$setValidity('tel', check);
						});
					}else{
						$ctrl.$setValidity('tel', true);
					}
				});
			}
		};
	}]);
});
