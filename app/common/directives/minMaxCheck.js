'use strict';
define(function(require , exports , module){
	
	function isEmpty(value) {
		  return angular.isUndefined(value) || value === '' || value === null || value !== value;
	}
	
	angular.module("ngMinMax" , [])
	.directive('ngMin' , function(){
	    return {
	        restrict: 'A',
	        require: 'ngModel',
	        link: function(scope, elem, attr, ctrl) {
	            scope.$watch(attr.ngMin, function(){
	            	if (ctrl.$viewValue) {
	            		ctrl.$setViewValue(ctrl.$viewValue);
	            	}
	            });
	            var minValidator = function(valueStr) {
	              var min = scope.$eval(attr.ngMin) || 0;
	              var value = parseInt(valueStr);
	              if (!isEmpty(value) && value < min) {
	                ctrl.$setValidity('min', false);
	                return undefined;
	              } else {
	                ctrl.$setValidity('min', true);
	                return value;
	              }
	            };

	            ctrl.$parsers.push(minValidator);
	            ctrl.$formatters.push(minValidator);
	        }
	    };
	})
	.directive('ngMax' , function(){
		  return {
		        restrict: 'A',
		        require: 'ngModel',
		        link: function(scope, elem, attr, ctrl) {
		            scope.$watch(attr.ngMax, function(){
		                if (ctrl.$viewValue) {
		                	ctrl.$setViewValue(ctrl.$viewValue);
		                }
		            });
		            var maxValidator = function(value) {
		              var max = scope.$eval(attr.ngMax) || Infinity;
		              if (!isEmpty(value) && value > max) {
		                ctrl.$setValidity('max', false);
		                return undefined;
		              } else {
		                ctrl.$setValidity('max', true);
		                return value;
		              }
		            };
	
		            ctrl.$parsers.push(maxValidator);
		            ctrl.$formatters.push(maxValidator);
		        }
		    };
	});	
});