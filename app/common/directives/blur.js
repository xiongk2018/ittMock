'use strict';
define(function(require , exports , module){
	angular.module("myBlur" , [])
	.directive('ngBlurctrl' , ['$http',function($http){
		return {
			require: 'ngModel',
			link:function($scope ,  $element , $attrs , $ctrl){
				$element.on('blur', function (evt) {
					$scope.$apply(function () {	
						var blurMethod = $attrs.ngBlurctrl;
						if(blurMethod){
							var objectName = blurMethod.slice(blurMethod.indexOf("(")+1,blurMethod.indexOf(")"));
							var methodName = blurMethod.slice(0,blurMethod.indexOf("("));							
							$ctrl.$setValidity('unique', $scope[methodName]($scope[objectName])?true:false);
						}	
					});
				});
			}
		};
	}]);
});