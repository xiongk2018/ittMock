define(function(require , exports , module){
	require("icheck-css");
	require("icheck");

	angular.module("uiCheck" , [])
	.directive('uiCheck' , ["$timeout" , function($timeout){
		return {
			restrict: 'A',
			require:'ngModel',
			replace: true,
			link:function($scope , $element , $attrs , ngModel){
					var value = $attrs["value"];

					$scope.$watch($attrs["ngModel"] , function(newValue){
						$element.iCheck("update");
					});

					return $element.iCheck({
						checkboxClass: 'icheckbox_square-blue',
					    radioClass: 'iradio_square-blue',
					    increaseArea: '20%' // optional
					}).on("ifChanged" , function(event){
	                    if ($element.attr('type') === 'checkbox' && $attrs['ngModel']) {
	                        $scope.$apply(function() {
	                            return ngModel.$setViewValue(event.target.checked);
	                        });
	                    }
	                    if ($element.attr('type') === 'radio' && $attrs['ngModel']) {
	                        return $scope.$apply(function() {
	                            return ngModel.$setViewValue(value);
	                        });
	                    }
					});
			}
		};
	}]);
});