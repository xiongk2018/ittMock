﻿'use strict';
define(function(require , exports){
	var tpl = require("../tpl/pagination.html"); 

	angular.module("uiPagination" , [])
	.directive('uiPagination' , [function($location){
		return {
			restrict: 'A',
			replace: true,
			template: tpl,
			controller: function(){},
			link:function($scope , $element , $attr){}
		}
	}]);
});