'use strict';
define(function(require , exports , module){
	angular.module("uniqueCheck2" , [])
	.directive('ngUnique2' , ['$http',function($http){
		return {
			require: 'ngModel',
			link:function($scope ,  $element , $attrs , $ctrl){
				
				$element.on('blur', function (evt) {				
					$scope.$apply(function () {	
						var ngModel = $scope.$eval($attrs.ngModel);
						
						var ngUnique = $attrs.ngUnique2.split('.');
						var $datas = $scope.$eval(ngUnique[0]);
						var fieldName = ngUnique[1];

						var sameSum = 0;
						for(var i in $datas){
					       if($datas[i][fieldName]==ngModel){
					    	   sameSum++;
					    	   if(sameSum>=2){ 
					    		   break;
					    	   }
					       }
						}
						$ctrl.$setValidity('unique', sameSum<2);
					});
				});
			}
		};
	}]);
});