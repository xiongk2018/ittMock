define(function(require , exports , module){
	require("scroller-css");
	require("scroller");

	angular.module("uiScroll" , [])
	.directive('uiScroll' , ["$timeout" , function($timeout){
		return {
			restrict: 'A',
			replace: true,
			link:function($scope , $element , $attrs){
				$timeout(function(){
					$element.jScrollPane({verticalGutter 	: 16 , 
						horizontalGutter:30 ,
						verticalDragMaxHeight:0});
					console.log("==============================");
				} , 1000);
			}
		};
	}]);
});