define(function(require , exports , module){
	require("datepicker-css");
	require("datepicker");

	$.fn.datetimepicker.dates['zh-CN'] = {
			days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
			daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
			daysMin:  ["日", "一", "二", "三", "四", "五", "六", "日"],
			monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
			months: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
			today: "今日",		
		suffix: [],
		meridiem: []
	};

	angular.module("uiDatepicker" , [])
	.directive('uiDatepicker' , ["$filter" , function($filter){
		return {
			restrict: 'A',
			replace: true,
			compile:function(){
				return function(scope, elm, attrs, controller) {
					var dataField    = attrs["linkField"];
					var startDateStr = attrs["startDate"];
					var endDateStr = attrs["endDate"];
					var format    = attrs["dateFormat"];
					var linkFormat    = attrs["linkFormat"];

					var startView =parseInt( attrs["startView"]||2);
					var minView = parseInt(attrs["minView"]||0);

					var textField = $("#" + dataField);

					var minDate = attrs["minDate"]||2;
					var startDate;
					var endDate;
					
					if (startDateStr !== "") {
						if (startDateStr === "now") {
							startDate = new Date();
						}
					}
					
					if(endDateStr !== ""){
						if(endDateStr === "now"){
							endDate = new Date();
						}
					}
					scope.$watch(textField.attr("ng-model") , function(current , old){
						textField.val($filter("date")(current , format));
					});
					var picker = elm.datetimepicker({
				        language:  'zh-CN',
				        dateFormat:format,
				        linkFormat:linkFormat,
				        linkField:dataField,
				        weekStart: 1,
				        todayBtn:  1,
						autoclose: 1,
						todayHighlight: 1,
						startView: startView,						
						minView: minView,
						startDate:startDate,
						endDate:endDate,
						forceParse: 0,
				        showMeridian: 1
				    });	
					
					if (startDateStr !== "" && startDateStr !== "now") {
						scope.$watch(startDateStr ,function(newValue , oldValue){
							elm.datetimepicker('setStartDate', newValue);
						} , true);
					}
					if (endDateStr !== "" && endDateStr !== "now") {
						scope.$watch(endDateStr ,function(newValue , oldValue){
							elm.datetimepicker('setEndDate', newValue);
						} , true);
					}
				};
			}
		};
	}]);
});