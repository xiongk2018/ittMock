define(function(require , exports , module){
	angular.module("idcardCheck" , [])
	.directive('ngIdcard' , ['$http',function($http){
		return {
			require: 'ngModel',
			link:function($scope ,  $element , $attrs , $ctrl){
				$element.on('blur', function (evt) {
					var value = $scope.$eval($attrs.ngModel);
					if(value!=null && value.length!=0){
						var check = /^[1-9]\d{5}[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}(\d|x|X)$/.test(value); 
						$scope.$apply(function(){
							 $ctrl.$setValidity('idCard', check);
						});
					}else{
						$ctrl.$setValidity('idCard', true);
					}
				});
			}
		};
	}]);
});
