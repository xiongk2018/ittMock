define(function(require , exports , module){
	require("timepicker-css");
	require("timepicker");

	angular.module("uiTimepicker" , [])
	.directive('uiTimepicker' , ["$filter" , function($filter){
		return {
			restrict: 'A',
			require:"ngModel",
			replace: true,
			link:function($scope, $elm, $attrs, ctrl){
				var picker = $elm.timepicker({
					minuteStep: 1,						
					showInputs: true,
					template: 'dropdown',
					showSeconds: false,
					showMeridian: false				
				});	
			
				if (!$elm.is("input")) {
					var field = $attr("linkField")
					picker.on('changeTime.timepicker', function(e) {
						$("#" + field).val(e.time.value);
						$scope.$apply();
					});
				}
				
				picker.on('hide.timepicker', function(e) {
					$scope.$apply(function(){
						var pickerMethod = $attrs.uiTimepicker;
						if(pickerMethod){
							var objectName = pickerMethod.slice(pickerMethod.indexOf("(")+1,pickerMethod.indexOf(")"));
							var hideMethod = pickerMethod.slice(0,pickerMethod.indexOf("("));							
							ctrl.$setValidity('unique', $scope[hideMethod]($scope[objectName])?true:false);
						}							
					});							
				});	
			}
		};
	}]);
});