'use strict';
define(function(require , exports){
	angular.module("uiTable" , [])
	.controller("dataTableCtrl" , function(){

	})
	.directive('uiTable' , ["$location" , "$http" , "$timeout" , "$dialogs" , "baseService" , "App" , function($location , $http , $timeout , $dialogs , baseService ,  App){
		return {
			restrict: 'A',
			replace: true,
			link:function($scope , $element , $attr){
			    var url = $attr["url"];
				var hasPage = $attr["hasPage"] || false;
			    var pageSize = $attr["pageSize"] || null;

				//加载数据函数
				function loadPageData(curPage , max) {
			    	var dataUrl = url.indexOf("?") == -1 ? url + "?" : url;
					var query = $scope.query;
					
					if (query) { 
						for (var name in query) {
							var value = query[name];
							if (value && value !== "") {
								dataUrl += "&q." + name + "=" + value;
							}
						}
					}
			 	  	baseService.request(dataUrl + "&page.curPage="+ curPage + "&page.max=" + max , "GET")
				  	.then(function(res){				  		
				  		var page = res;
				  		var pageNum = [];
				  		var showPage = 5;
				  		var startPage = page.curPage - 2 > 0?page.curPage - 2 : 1; 
				  		for (var i = startPage;i < startPage + 5; i++) {
				  			if ( i <= page.pageCount) {
				  				pageNum.push(i);
				  			}
				  		}
				  		
				  		$scope.list = page.datas;
				  		$scope.page = page;
				  		$scope.pageNum = pageNum;
				  		
				  		$scope.$watch("page" ,function(newValue , oldValue){
					  		var showPage = 5;
					  		var newStartPage = newValue.curPage - 2 > 0?newValue.curPage - 2 : 1; 
					  		var newPageNum = [];
					  		for (var i = newStartPage;i < newStartPage + 5; i++) {
					  			if ( i <= newValue.pageCount) {
					  				newPageNum.push(i);
					  			}
					  		}
					  		
					  		$scope.pageNum = newPageNum;
				  		} , true)
				  	});	    	 
			    };	

				function loadData(url) {
					return baseService.request(url , "GET");
				}

				function renderTable($scope , $element , $timeout) {
					$timeout(function(){
						var bFilter = !($element.is("[data-no-filter]")?true:false);
						var bLengthChange = !($element.is("[data-no-length]")?true:false);
						var bPaginate = !($element.is("[data-no-paginate]")?true:false);
						var bInfo = !($element.is("[data-no-infoDisplay]")?true:false);
					
						var cols = $element.find("thead th");
						var columnDef = new Array();
						angular.forEach(cols , function(item){
							var def = {};
							var sortable = $(item).attr("sortable")
							if (sortable) {
								def.bSortable = sortable === "false"?false : [sortable];
							}
							else {
								def.bSortable = null;
							}
							columnDef.push(def);
						});
											
						var table = $element.dataTable({							
							oLanguage:{
								bDestroy:true,
								sSearch:"搜索",
								sLengthMenu:" 每页显示 _MENU_ 条记录",
								sZeroRecords:"没有找到记录",
								sInfo:" 显示第 _START_ 条到第 _END_ 条记录 一共 _TOTAL_ 条",
								sInfoEmpty:" 显示0条记录",
								oPaginate:{
									sPrevious:"上一页",
									sNext:"下一页"
								}
							},
							bFilter:bFilter,
							bLengthChange:bLengthChange,
							bPaginate:bPaginate,
							bInfo:bInfo,
							aoColumns: columnDef
						});						
					});
				}
				
				if (hasPage) {					
					$scope.loadPageData = loadPageData;
					if(pageSize){
						loadPageData(1 , Number(pageSize));
					}else{
						loadPageData(1 , 10);
					}										
				}
				else {
					if(url && url!==""){
						loadData(url)
						.then(function(res){
							$scope.list = res.datas?res.datas:res;
							renderTable($scope , $element , $timeout);
						});
					}
					else{
						renderTable($scope , $element , $timeout);
					}
				}		
			}
		}
	}]);
});