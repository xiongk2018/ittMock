define(function(require , exports , module){
	angular.module("ngIp" , [])
	.directive('ngIp' , ['$http',function($http){
		return {
			require: 'ngModel',
			link:function($scope ,  $element , $attrs , $ctrl){
				$element.on('blur', function (evt) {
					var value = $scope.$eval($attrs.ngModel);
					if(value!=null && value.length!=0){
						var check =  /((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))$/.test(value); 
						$scope.$apply(function(){
							 $ctrl.$setValidity('Ip', check);
						});
					}else{
						$ctrl.$setValidity('Ip', true);
					}
				});
			}
		};
	}]);
});
