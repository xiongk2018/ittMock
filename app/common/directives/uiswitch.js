define(function(require , exports , module){

	angular.module("uiSwitch" , [])
	.directive('uiSwitch' , ["$timeout" , function($timeout){
		return {
			restrict: 'A',
			replace:false,
			compile:function(){
				return function(scope, elm, attrs) {
					elm.addClass("checked=''").addClass("itt").addClass("itt-switch").addClass("itt-switch-5");
					$("<span class='lbl'></span>").insertAfter(elm);
				};
			}
		};
	}]);
});