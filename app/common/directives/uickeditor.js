/**
 * ckeditor Directive
 * @author ��С��
 */

	
define(function(require , exports , module){
	require("ckeditor");
	
	angular.module("uickeditor" , [])
	.directive('uickeditor' , [function(){
		return {
			restrict: 'A',
			require:"ngModel",
			replace: true,
			link:function($scope, $elm, $attrs, ngModel){ 
				
				var ckeditor = CKEDITOR.replace($elm[0], {  height: "600px" , width: "100%" ,allowedContent: true });
				
				if (!ngModel) {
					return;
				}
							
				ckeditor.on('instanceReady', function() {
					ckeditor.setData(ngModel.$viewValue);
				});
				ckeditor.on('pasteState', function() {
					//$scope.$apply(function() {
						ngModel.$setViewValue(ckeditor.getData());
					//});
				});
				ngModel.$render = function(value) {
					ckeditor.setData(ngModel.$viewValue);
				};

				$scope.$watch($attrs.ngModel,function(newValue,oldValue){
					if ( ckeditor.mode == 'wysiwyg' )
					{				
						//ckeditor.setData(null);
						ckeditor.setData(newValue);
					}					
				});
				
			}
		};
	}]);
});