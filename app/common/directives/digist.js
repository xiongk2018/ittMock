'use strict';
define(function(require , exports , module){
	angular.module("digist" , [])
	.directive('ngDigist' , [function(){
		return {
			require: 'ngModel',
			link:function($scope ,  $element , $attrs , $ctrl){					
				$scope.$watch($attrs.ngModel,function(newValue,oldValue){
					var numberReg =  /^-?\d+\.?\d*$/;
					
					if(!newValue || numberReg.test(newValue)) {
						$ctrl.$setValidity('digist', true);
					}
					else{
						$ctrl.$setValidity('digist', false);
					}
				});
			}
		};
	}]); 
});