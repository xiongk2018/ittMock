'use strict';
define(function(require , exports , module){
	angular.module("integer" , [])
	.directive('ngInteger' , [function(){
		return {
			require: 'ngModel',
			//template : '<div onkeyup="value=value.replace(/[^\a-\z\A-\Z0-9]/g,'')"></div>',
			link:function($scope ,  $element , $attrs , $ctrl){					
				$scope.$watch($attrs.ngModel,function(newValue,oldValue){
					var numberReg =  /^\+?[1-9][0-9]*$/;
					if(!newValue || numberReg.test(newValue)) {
						$ctrl.$setValidity('integer', true);
					}
					else{
						$ctrl.$setValidity('integer', false);
					}
				});
			}
		};
	}]); 
});