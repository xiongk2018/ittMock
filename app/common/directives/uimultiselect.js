define(function(require , exports , module){
	require("multiselect");
	//require("multiselect-main");
	require("multiselect-css");

	angular.module("uiMultiselect" , [])
	.directive('uiMultiselect' , ["$filter" , function($filter){
		return {
			restrict: 'A',
			replace: true,
			compile:function(){
				return function($scope, $elm, $attrs, controller) {				
					$elm.multiselect2side({
						selectedPosition: 'right',
						moveOptions: false,
						labelsx: '待选区',
						labeldx: '已选区'
					});
				};
			}
		};
	}]);
});