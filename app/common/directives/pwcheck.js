define(function(require , exports , module){
	angular.module("pwCheck" , [])
	.directive('pwCheck' , [function(){
		return {
			require: 'ngModel',
			link:function($scope , $element , $attrs , $ctrl){
				var firstPassword = '#' + $element.attr("pw-check");				
				$element.add(firstPassword).on('keyup', function () {
					$scope.$apply(function () {
						var v = $element.val()===$(firstPassword).val();
						$ctrl.$setValidity('pwmatch', v);
					});
				});
			}
		
		};
	}]);
});