'use strict';
define(function(require , exports , module){
	angular.module("myValidate" , [])
	.directive('ngValidate' , ['$http',function($http){
		return {
			require: 'ngModel',
			link:function($scope ,  $element , $attrs , $ctrl){
				var fieldName = $attrs.name;
				if (!$scope.$parent.validate) {
					$scope.$parent.validate = {};
				}				
				$element.on('keyup', function (evt) {	
					$scope.$apply(function () {				
						$scope.$parent.validate[fieldName] = {};			
						
						var errorObj = $ctrl.$error;
						var error = {};
						for ( var key in errorObj ){							
							if (errorObj[key]){								
								if ( key == 'required' ){
									error.required = true;
								}
								else if ( key == 'minlength' ){
									error.minlength = true;
								}								
								else if ( key == 'maxlength' ){
									error.maxlength = true;
								}
								else if ( key == 'min' ){
									error.min = true;
								}								
								else if ( key == 'max' ){
									error.max = true;
								}
								else if ( key == 'email' ){
									error.email = true;
								}								
								else if ( key == 'url' ){
									error.url = true;
								}
								else if ( key == 'pattern' ){
									error.pattern = true;
								}		
							}
						}
						$scope.$parent.validate[fieldName].error = error;
						console.log($scope.$parent.validate)
					});
				});
			}
		};
	}]);
});