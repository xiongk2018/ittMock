'use strict';
define(function(require , exports , module){
	angular.module("uniqueCheck" , [])
	.directive('ngUnique' , ['$http',function(http){
		return {
			require: 'ngModel',
			link:function(scope ,  element , attrs , ctrl){				
				element.on('blur', function (evt) {				
					scope.$apply(function () {	
						var ngModel = scope.$eval(attrs.ngModel);
						var ngUnique = attrs.ngUnique;
						var remote = ngUnique.replace(/\$v+/g , ngModel);
						var ajaxConfiguration = { method: 'get', url: remote};
						http(ajaxConfiguration)
			            .success(function(data, status, headers, config) {
							var exist = data?true:false;
			                ctrl.$setValidity('unique', !exist);
			            });
					});
				});
			}
		};
	}]);
});