define(function(require , exports , module){
	require("wizard");

	angular.module("uiWizard" , [])
	.directive('uiWizard' , ["$dialogs" , function($dialogs){
		return {
			restrict: 'A',
			replace: true,
			compile:function(){
				return function(scope, elm, attrs, controller) {
					var change = attrs["change"];
					var finished    = attrs["finished"];
					app_wizard(elm).on("change" , scope.$eval(change))
					.on("finished" , scope.$eval(finished));		
				};
				
				function app_wizard(e){
					e.wizard();
					var d = e.siblings(".wizard-actions").eq(0);
					var f = e.data("wizard");
					f.$prevBtn.remove();
					f.$nextBtn.remove();
					f.$prevBtn = d.find(".btn-prev").eq(0).on("click" , function(){
						var dlg = $dialogs.confirm("确认返回" , "返回上一步将丢失已修改数据 ,是否确认?");
						dlg.result.then(function(){
							e.wizard("previous");	
						});
					}).attr("disabled" , "disabled");
					f.$nextBtn = d.find(".btn-next").eq(0).on("click" , function(){
						e.wizard("next");
					}).removeAttr("disabled");
					f.nextText = f.$nextBtn.text();
					return e;
				};
			}
		};		
	}]);
});