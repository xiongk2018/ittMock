'use strict';
define(function(require , exports){

	var tpl = require("../tpl/warn.html"); 
	angular.module("warnLoad" , ["ngRoute" , "App"])
	.controller('warnLoadCtrl' , function($scope , $filter , $dialogs  , baseService , App){
		getWarnInfo();
		getMessageInfo();
		$scope.init = function(){
			   $("#alarm-container").hide();
			   $("#settings-btn").click(function(){
					$("#alarm-container").slideToggle();
					$("#settings-btn").find("i").toggleClass("icon-double-angle-down");
		       });
		 	   $(".icon-cog").click(function(){
		   			$(".alarm-setting").animate({
		   				left:'toggle'
		   			});
		   			$(".info-setting").animate({
		   				left:'toggle'
		   			});
				})
		   		$(".alarm-setting").click(function(){
		   			$(".info-setting").removeClass("color-setting");
		   			$(".alarm-setting").addClass("color-setting");
		   			$("#home").show();
		   			$("#profile").hide();
		   		})
		   		$(".info-setting").click(function(){
		   			$(".info-setting").addClass("color-setting");
		   			$(".alarm-setting").removeClass("color-setting");
		   			$("#home").hide();
		   			$("#profile").show();
		   		})
		}
		App.addListener('PERSISTENCE_Warn' , function(e , p){
			loadWarnData();
		});
		function getWarnInfo(){
		       loadWarnData();
				/*App.addListener('PERSISTENCE_Warn' , function(e , p){
					loadWarnData();
				});*/
		   		$scope.showWarn = function(id){
		  		   var dlg = $dialogs.create('./app/operation/warnMessage/tpl/warnUpdate.html',function($scope , $modalInstance , data , baseService , $location, $route){
						 var id = data.id;
						 var page = data.page||null;
						 var loadPageData = data.loadPageData||null;
						
						 baseService.modify("warn/read/"+id,"PUT",{},true,function(){})
						 .then(function(res){
							 $scope.warn = res.params.Warn;
							 if(page){
								 loadPageData(page.curPage , page.max);
							 }
						 });
						 $scope.update = function(warn){
							 baseService.modify("warn/close/"+warn.id,"PUT",{"remark":warn.remark})
							 .then(function(res){
								 if(page){
									 loadPageData(page.curPage , page.max);
								 }
								 $modalInstance.close();
							 });
						 }
						 $scope.cancel = function(){
							 $location.path($location.path());
							 $modalInstance.dismiss('canceled');  
						 }
						},
						{id:id},{key: false,back: 'static'});
		 	 	}
		   		/*function loadWarnData(){
			   		 baseService.request("/warn?orders[createDate]=desc&q.status:eq=uncheck","GET")
			 		   .then(function(data){
			 				 $scope.list = data.datas;
			 		 });
		   		}	*/
	       }
		function loadWarnData(){
			 var route = $scope.route;
			 //监听线路
			 $scope.$watch("route" , function(newValue){
				 if (newValue){
					 baseService.request("/warn?orders[createDate]=desc&q.status:eq=uncheck&q.lineCode:eq=" + newValue.code,"GET")
						 .then(function(data){
							 $scope.list = data.datas;
						 });
				 }
				 else{
					 baseService.request("/warn?orders[createDate]=desc&q.status:eq=uncheck","GET")
						 .then(function(data){
							 $scope.list = data.datas;
						 });

				 }
			 } , true);
		 }
		function getMessageInfo(){
		       loadMessageData();
				App.addListener('PERSISTENCE_Warn' , function(e , p){
					loadMessageData();
				});
		   		$scope.showMessage = function(id){
		  		   var dlg = $dialogs.create('./app/operation/warnMessage/tpl/notifyUpdate.html','notifyUpdateCtrl',{id:id},{key: false,back: 'static'});
		 	 	}
		   		function loadMessageData(){
			   		 baseService.request("/simpleMessage?orders[createDate]=desc&q.status:eq=uncheck","GET")
			 		   .then(function(data){
			 			$scope.data = data.datas;
			 		 });
		   		}
	       }
	})
	.directive('warnLoad' , ["$location" , function($location){
		return {
			restrict: 'A',
			replace: true,
			template: tpl,
			controller: "warnLoadCtrl",
			link:function($scope , $element , $attr , ngModel){}
		}
	}]);
});