'use strict';
define(function(require , exports , module){
	angular.module("hasPermission",[])
	.directive('hasPermission' ,["App" , function(App){
		return {
			link: function(scope, element, attrs) {				  
			      if(!angular.isString(attrs.hasPermission))
			        throw "hasPermission value must be a string";			  
			      var value = attrs.hasPermission.trim();
			      function toggleVisibilityBasedOnPermission() {
			        var hasPermission = App.hasUserPermission(value);	
			        if(hasPermission)
			          element.show();
			        else
			          element.hide();
			      }
			      toggleVisibilityBasedOnPermission();
			    }
		};
	}]);
});

