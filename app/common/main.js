'use strict';
define(function(require , exports){
	require("ui-bootstrap");
	require("directives-dialog");
	require("store");
	require("./common");
	require("./services/app");
	require("./services/base");

	require("wizard");
	require("messenger");
	require("messenger-css");
	require("messenger-theme-future-css");
	require("messenger-theme-future");
	require("validate");
	require("dataTable");
	require("dataTable-bootstrap");
	require("bootbox");
	require("select2-css");
	require("select2-b-css");
	require("select2-locale");
	require("select2");
	require("datepicker-css");
	require("datepicker");
	require("ztree-css");
	require("ztree");

	require("ckeditor");	
	
	require("timepicker-css");
	require("timepicker");
	require("store");
	require("./directives/nav");
	require("./directives/uitable");
	require("./directives/uicheck");
	require("./directives/uiselect");
	require("./directives/uidatepicker");
	require("./directives/uiztree");
	require("./directives/uitimepicker");
	require("./directives/uiWizard");
	require("./directives/pwcheck.js");
	require("./directives/uniqueCheck.js");
	require("./directives/uniqueCheck2.js");
	require("./directives/idcardCheck.js");
	require("./directives/uitreetable.js");
	require("./directives/uipagination.js");
	require("./directives/uiswitch.js");
	require("./directives/blur.js");
	require("./directives/minMaxCheck.js");
	require("./directives/telCheck.js");
	require("./directives/ipCheck.js");
	require("./directives/uiselectree.js");
	require("./directives/validate.js");
	require("./directives/hasPermission.js");
	require("./directives/integer.js");
	require("./directives/uickeditor.js");
	require("./directives/warnLoad.js");
	
	angular.module("common" , ["ui.bootstrap" , "dialogs" , "App" , "base" , "headerNav", "uiTable" , "uiCheck" ,"uiSwitch", "uiSelect" , "uiDatepicker", "uiZtree" , "uiTimepicker","pwCheck",

			"uniqueCheck","uniqueCheck2","idcardCheck" , "uiTreetable" , "uiPagination" ,"uiSelectree", "uiWizard","myBlur" , "myValidate","ngMinMax" ,"uickeditor","ngTel" ,"ngIp" , "hasPermission","integer","warnLoad"]);
});