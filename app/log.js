var log4js = require('log4js');
log4js.configure({
  appenders: [
	{
      type: 'dateFile', 
      filename: 'logs/log.log', 
	  pattern: '_yyyy-MM-dd',
	  alwaysIncludePattern : false,
      category: 'dateFileLog' 
    }
  ],
  replaceConsole: false,
  levels: {
        dateFileLog: 'WARN'
		}
});

var dateFileLog = log4js.getLogger('dateFileLog');
exports.logger = dateFileLog;







