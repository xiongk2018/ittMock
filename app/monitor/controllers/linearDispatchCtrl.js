﻿'use strict';
var dtRunExecutor = require("./app/mock/executor/dtRunExecutor.js");
var eventObj = require("./app/mock/event");
define(function(require , exports , module){
	require("kinetic");
	require("store");
	var linearDispatch = require("../linearDispatch/linearDispatch.js");
	//dtRunExecutor.init();
	module.exports = function(app){
		//linear dispatch==================================================================================
		app.register.controller("linearDispatchCtrl" ,function($rootScope , $scope ,$timeout ,App,$dialogs,$filter,$http){
			$scope.linears = new Map();
			$scope.selectedType = "Default";//点击图形，选择的图形类型
			$scope.controlContainerShow = false;//操作区是否显示
			$scope.selectedRouteCode=null;//选择进路
			$scope.isCommandding = false ; //是否正在操作操作区
			$scope.shapeType={            //图形类型
				"Segment" :"Segment",
				"Turnout" : "Turnout",
				"RouteIndicator":"RouteIndicator",
				"Crossing" : "Crossing",
				"TurnoutIndicator" :"TurnoutIndicator"
			};
			$http({
				method:"GET",
				url:"./datas/route.json"
			})
			.then(function(res){
					$scope.routeAll = res.data;
			});

			if ($rootScope.lines && $rootScope.lines.length > 0) {
				$scope.selectedLine = $rootScope.lines[0];
				$scope.$parent.route = $rootScope.lines[0];
			}
			$scope.selectLine = function(line) {
				$scope.selectedLine = line;
				$scope.$parent.route = line;
			};
			/**
			 * 点击区段监听事件
			 */
			App.addListener(linearDispatch.Linear.EVENT_SELECTED_SEGMENT , function(e , data) {
				$scope.segment = data.data;
				$scope.controlContainerShow = true;
				$scope.selectedType = $scope.shapeType.Segment;
				$scope.$apply();
			});
			/**
			 * 点击道岔监听事件
			 */
			App.addListener(linearDispatch.Linear.EVENT_SELECTED_TURNOUT , function(e , data) {
				$scope.turnout = data.data;
				$scope.controlContainerShow = true;
				$scope.selectedType = $scope.shapeType.Turnout;
				$scope.$apply();
			});
			/**
			 * 点击进路表示器监听事件
			 */
			App.addListener(linearDispatch.Linear.EVENT_SELECTED_ROUTEINDICATOR, function(e , data) {
				$scope.routeIndicator = data;
				$scope.controlContainerShow = true;
				$scope.selectedType = $scope.shapeType.RouteIndicator;
				//console.log(JSON.stringify(App.Route));
				//$scope.routeList = $filter("filter")(App.Route , {inIndicatorCode:$scope.routeIndicator.code},true);
				$scope.routeList = $filter("filter")($scope.routeAll , {inIndicatorCode:$scope.routeIndicator.code},true);
				$scope.$apply();
			});
			/**
			 * 点击平交道口监听事件
			 */
			App.addListener(linearDispatch.Linear.EVENT_SELECTED_CROSSING, function(e , data) {
				$scope.crossing = data.data;
				$scope.controlContainerShow = true;
				$scope.selectedType = $scope.shapeType.Crossing;
				$scope.$apply();
			});
			/**
			 * 点击道岔表示器监听事件
			 */
			App.addListener(linearDispatch.Linear.EVENT_SELECTED_TURNOUTINDICATOR, function(e , data) {
				$scope.turnoutIndicator = data.data;
				$scope.controlContainerShow = true;
				$scope.selectedType = $scope.shapeType.TurnoutIndicator;
				$scope.$apply();
			});
			$scope.$watch('selectedRouteCode',function(newValue,oldValue){
				if(newValue===null || newValue === oldValue ){
					return;
				}
				var routeItems = _.findWhere($scope.routeList,{"code":newValue}).routeItems ;
				if ($scope.linear) {
					$scope.isCommandding = true;
					$scope.linear.selectOneRoute(routeItems);
				}
			});
			$scope.closeControlContainer = function(){
				if($scope.isCommandding){
					$dialogs.notify("正在操作，请先完成操作再关闭");
				}
				$scope.controlContainerShow = false;
			};

			$scope.sendCommand = function( code , statusType ){
				if ($scope.linear) {
					$scope.linear.cancelSelectedRoute();
				}
				$scope.isCommandding = false;
			};

			/**
			 * 列车实时数据监听
			 */
			eventObj.event.on('tram_status_change_event', function(data) {
				//console.log("get message -----tram status");
				var selectedLine = $scope.selectedLine;
				var linear = $scope.linear;
				if (linear && data.length > 0) {
					linear.renderTrams(data);
				}
			});
			eventObj.event.on('device_status_change_event', function(data) {
				//console.log("get message -----device status");
				var selectedLine = $scope.selectedLine;
				var linear = $scope.linear;
				if (linear && data.length > 0) {
					linear.deviceChange(data);
				}
			});
			
			$scope.line = {};

		});

		app.register.directive("linear" , function($timeout , baseService , App , $http){
			return {
				require:"ngModel",
				link:function($scope , $element , $attrs , ngModel){
					 $http({
						method:"GET",
						url:"./datas/linearDispatchData.json"
					})
						.then(function(res){
							var  linearDispatchData = res.data[0] ;
							var line = ngModel.$modelValue;
							var hasRendered = false;
							$scope.$watch($attrs.ngShow , function(newValue){
								if (!hasRendered && newValue) {

									var config = linearDispatchData.stage;
									var linear = new linearDispatch.Linear(line);
									linear.init($element.attr("id"), config, linearDispatchData);
									$scope.linear = linear;
									linear.selectedSegmentCallback = function (data) {
										$scope.$emit(linearDispatch.Linear.EVENT_SELECTED_SEGMENT, data);
									};
									linear.selectedTurnoutCallback = function (data) {
										$scope.$emit(linearDispatch.Linear.EVENT_SELECTED_TURNOUT, data);
									};
									linear.selectedRouteIndicatorCallback = function (data) {
										$scope.$emit(linearDispatch.Linear.EVENT_SELECTED_ROUTEINDICATOR, data);
									};
									linear.selectedCrossingCallback = function (data) {
										$scope.$emit(linearDispatch.Linear.EVENT_SELECTED_CROSSING, data);
									};
									linear.selectedTurnoutIndicatorCallback = function (data) {
										$scope.$emit(linearDispatch.Linear.EVENT_SELECTED_TURNOUTINDICATOR, data);
									};
								}
							});

						});
				}
			};
		});
	}
});