'use strict';
define(function(require , exports){
	require("kinetic");
	require("store");
	require("./services/linearDispatchService.js");
	require("./services/dispatchService.js");
	require("./graphics/trams.js");
	require("./graphics/shapeItem.js");
	require("./graphics/turnoutItem.js");
	require("./graphics/shapeItemEdit.js");
	require("./panoramic/panoramic.js");
	require("./linearDispatch/linearDispatch.js");
	require("./linearDispatch/dispatch.js");
	require("./linearDispatch/linearDispatchEdit.js");
	require("./topologyMap/topologyMap.js");
	require("./turnoutMap/turnoutMap.js");
	require("./replay/replay.js");
	require("./controllers/monitorCtrl.js");
	require("./controllers/tramsStatusCtrl.js");
	require("./controllers/linearDispatchEditCtrl.js");
	require("./controllers/turnoutOperationCtrl.js");
	
	angular.module("monitor" , ["ngRoute" , "common" , "monitorCtrl" , "tramsStatusCtrl","editCtrl","turnoutOperation"])
	.config(["$routeProvider" , function($routeProvider){
		$routeProvider.when("/panoramicMonitor" , {
			templateUrl:"./app/monitor/tpl/panoramic.html",
			controller: "panoramicCtrl"
		})
		.when("/linearDispatchMonitor" , {
			templateUrl:"./app/monitor/tpl/linearDispatch.html",
			controller: "linearDispatchCtrl"
		})	
		.when("/dispatchMonitor" , {
			templateUrl:"./app/monitor/tpl/dispatch.html",
			controller: "dispatchCtrl"
		})	
		.when("/linearDispatchReplay" , {
			templateUrl:"./app/monitor/tpl/linearReplay.html",
			controller: "linearDispatchReplayCtrl"
		})
		.when("/tramsStatus" , {
			templateUrl:"./app/monitor/tpl/tramsStatus.html",
			controller:"statusCtrl"
		})
		.when("/linearDispatchEdit" , {
			templateUrl:"./app/monitor/tpl/linearDispatchEdit.html",
			controller: "linearDispatchEditCtrl"
		})
		.when("/turnoutOperation" , {
			templateUrl:"./app/monitor/tpl/turnoutOperation.html",
			controller: "turnoutOperationCtrl"
		})
		.otherwise({
			redirectTo:"/panoramicMonitor"
		});

	}]);
});