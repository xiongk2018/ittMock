/**
 *  全景图绘制对象
 *
 **/
'use strict';
define(function(require , exports , module){
	var shape = require("./graphics/shape.js");
	/**
	 * 线路绘制对象
	 * 其中包含了直线调度中包括线路、进路表示器等元素的绘制
	 */
	var Linear = function(line) {
		this.stage;
		this.layer = new Kinetic.Layer();
		this.tempLayer =  new Kinetic.Layer();
		this.width;
		this.height;
		this.sectionMap = new Map();
		this.stationMap = new Map();
		this.sectionStationMap = new Map();
		this.indicatorMap = new Map();

		this.turnoutMap = new Map();
		this.tramMap = new Map();
		this.deviceMap =  new Map();
		this.isShow = true;
		this.line = line;
		this.selectedTram;
		this.selectedStation;
		this.config;
		this.shapeType = {
			section : "Section",
			terminus : "Terminus",
			marking : "Marking",
			crossing : "Crossing",
			turnout : "Turnout" ,
			routeIndicator: "RouteIndicator",
			turnoutIndicator : "TurnoutIndicator",
			Stage:"Stage"
		};
		this.downUpType = {
			UP:"up",
			DOWN:"down"
		};
		this.deviceType = {
			Indicator :"INDICATOR" ,
			Turnout   : "TURNOUT"
		};
	};
	Linear.EVENT_SELECTED_TRAM = "linear.selectedTram";
	Linear.EVENT_SELECTED_TERMINUS = "linear.selectedTerminus";
	Linear.EVENT_SELECTED_SECTION= "linear.selectedSection";
	Linear.EVENT_SELECTED_SEGMENT= "linear.selectedSegment";
	Linear.EVENT_SELECTED_TURNOUT = "linear.selectedTurnout";
	Linear.EVENT_SELECTED_ROUTEINDICATOR = "linear.selectedRouteIndicator";
	Linear.EVENT_SELECTED_TURNOUTINDICATOR = "linear.selectedTurnoutIndicator";
	Linear.EVENT_SELECTED_CROSSING = "linear.selectedCrossing";
	/**
	 * 初始化
	 * @param container
	 * @param config
	 * @param datas
	 */
	Linear.prototype.init = function(container , config , datas) {
		this.width = config.width;
		this.height = config.height;
		this.config = config;
		this.datas = datas;
		this.stage = new Kinetic.Stage({
			container:container,
			width:this.width,
			height:this.height,
			draggable:false,
			offset:{x:-100 , y:90}
		});
		this.stage.add(this.layer);
		this.stage.add(this.tempLayer);
		this.renderData(this.datas);//加载静态数据
	};
	/**
	 * 放大
	 */
	Linear.prototype.zoomIn = function(){
		if(this.stage.scaleX() >=  1.1*1.1*1.1){
			return ;
		}else{
			this.stage.scale({x:this.stage.scaleX()*1.1,y:this.stage.scaleY()*1.1});
			this.stage.width(this.stage.width()*1.1);
			this.stage.height(this.stage.height()*1.1);
		}

	};
	/**
	 * 缩小
	 */
	Linear.prototype.zoomOut = function(){
		if(this.stage.scaleX() <= 1){
			return ;
		}else{
			this.stage.scale({x:this.stage.scaleX()/1.1,y:this.stage.scaleY()/1.1});
			this.stage.width(this.stage.width()/1.1);
			this.stage.height(this.stage.height()/1.1);
		}
	};
	/**
	 * 渲染静态数据
	 * @param datas
	 */
	Linear.prototype.renderData = function(datas) {
		var $this = this ;
		var sections = datas.sections;
		var segments = datas.segments;
		var turnouts = datas.turnouts;
		var routeIndicators = datas.routeIndicators;
		var terminuses = datas.terminuses;
		var crossings = datas.crossings;
		var endNodes = datas.endNodes;
		var turnoutIndicators = datas.turnoutIndicators;
		var markings = datas.markings;
		_.each(turnoutIndicators,function(turnoutIndicator){
			$this.renderTurnoutIndicator(turnoutIndicator);
		});
		_.each(routeIndicators,function(routeIndicator){
			$this.renderRouteIndicator(routeIndicator);
		});

		_.each(terminuses,function(terminus){
			$this.renderTerminus(terminus);
		});

		_.each(segments,function(segment){
			$this.renderSegment(segment);
		});
		_.each(sections,function(section){
			$this.renderSection(section);
		});

		_.each(crossings,function(crossing){
			$this.renderCrossing(crossing);
		});
		_.each(turnouts,function(turnout){
			if(turnout.parentId==null){
				turnout.children=[];
				$this.turnoutMap.put(turnout.id , turnout);
			}else{
				$this.turnoutMap.get(turnout.parentId).children.push(turnout);
			}
		});
		_.each($this.turnoutMap.values(),function(turnout){
			$this.renderTurnout(turnout);
		});
		_.each(endNodes , function(endNode){
			$this.renderEndNode(endNode);
		});
		_.each(markings,function(marking){
			$this.renderMarking(marking);
		});
		this.layer.batchDraw();

	};

	//渲染站台
	Linear.prototype.renderTerminus = function(terminus) {
		var terminusShape = new shape.Terminus(terminus , this.layer);
		terminusShape.draw();
		terminusShape.setProperty("S");
		this.deviceMap.put(terminus.code , terminusShape);
	};
	//渲染区段
	Linear.prototype.renderSection = function(section) {
		var $this = this;
		var sectionShape = new shape.Section(section , this.layer);
		if(section.parentId == null){
			sectionShape.drawText();
		}else{
			sectionShape.drawLine();
			//	sectionShape.drawText();
			//	sectionShape.updateStatus(0);
			this.deviceMap.put(section.code , sectionShape);
			(function(section){
				sectionShape.click(function(){
					if ($this.selectedSectionCallback) {
						$this.selectedSectionCallback(section);
					}
				});
			})(section);
		}
	};
	//渲染区段
	Linear.prototype.renderSegment = function(segment) {
		var $this = this;
		var segmentShape = new shape.Segment(segment , this.layer);
		/*	if(segment.parentId == null){
		 segmentShape.drawText();
		 }else{*/
		segmentShape.drawLine();
		segmentShape.drawText();
		this.deviceMap.put(segment.code , segmentShape);
		(function(segment){
			segmentShape.click(function(){
				if ($this.selectedSegmentCallback) {
					$this.selectedSegmentCallback(segment);
				}
			});
		})(segment);

		//}
	};
	//渲染道岔
	Linear.prototype.renderTurnout = function(turnout) {
		var $this = this ;
		var turnoutShape = new shape.Turnout(turnout , this.layer);
		turnoutShape.draw();
		this.deviceMap.put(turnout.code , turnoutShape);
		(function(turnout){
			turnoutShape.click(function(){
				if ($this.selectedTurnoutCallback) {
					$this.selectedTurnoutCallback(turnout);
				}
			});
		})(turnout);
	};
	//渲染平交道口
	Linear.prototype.renderCrossing = function(crossing){
		var $this = this ;
		var crossingShape = new shape.Crossing(crossing,this.layer);
		crossingShape.draw();
		//	crossingShape.updateStatus(0);
		this.deviceMap.put(crossing.code , crossingShape);
		(function(crossing){
			crossingShape.click(function(){
				if ($this.selectedCrossingCallback) {
					$this.selectedCrossingCallback(crossing);
				}
			});
		})(crossing);
	};
	//渲染进路表示器
	Linear.prototype.renderRouteIndicator = function(routeIndicator){
		var $this = this ;
		var routeIndicatorShape = new shape.RouteIndicator(routeIndicator , this.layer);
		routeIndicatorShape.draw();
		this.deviceMap.put(routeIndicator.code , routeIndicatorShape);
		(function(routeIndicator){
			routeIndicatorShape.click(function(){
				if ($this.selectedRouteIndicatorCallback) {
					$this.selectedRouteIndicatorCallback(routeIndicator);
				}
			});
		})(routeIndicator);
	};
	//渲染道岔表示器
	Linear.prototype.renderTurnoutIndicator = function(turnoutIndicator){
		var $this = this ;
		var turnoutIndicatorShape  =  new shape.TurnoutIndicator(turnoutIndicator , this.layer);
		turnoutIndicatorShape.draw();
		this.deviceMap.put(turnoutIndicator.code , turnoutIndicatorShape);
		(function(turnoutIndicator){
			turnoutIndicatorShape.click(function(){
				if ($this.selectedTurnoutIndicatorCallback) {
					$this.selectedTurnoutIndicatorCallback(turnoutIndicator);
				}
			});
		})(turnoutIndicator);
	};
	/**
	 * 渲染尽头线
	 * @param endNode
	 */
	Linear.prototype.renderEndNode = function(endNode){
		var endNodeShape = new shape.EndNode(endNode , this.layer);
		endNodeShape.draw();
	};
	/**
	 * 渲染辅助线
	 * @param marking
	 */
	Linear.prototype.renderMarking = function(marking){
		var markingShape = new shape.Marking(marking , this.layer);
		markingShape.draw();
	};
	//渲染站台
	Linear.prototype.renderTrams = function(trams) {
		//tram数据结构 ：lineCode、code、segment、serviceNum
		var $this = this;
		for(var i = 0 ;i<trams.length;i++){
			try{
				var tram = trams[i];
				tram.direction = tram.direct;
				tram.serviceNum=tram.serviceNum;
				tram.time='20';
				if(tram.tramCode!=null && tram.currentSegment.code!=null){
					var segmentShape = $this.deviceMap.get(tram.currentSegment.code);
					if(segmentShape.code!=null){
						var segmentX = segmentShape.data.x;
						var segmentY = segmentShape.data.y;
						var segmentDirection = segmentShape.data.direction;
						var segmentLen = segmentShape.data.length;
						//		tram.offset = (tram.direction === $this.downUpType.UP ? {x:0,y:65} :{x:0,y:-95});
						if(!segmentShape.data.tramPos){
							if(segmentDirection == this.downUpType.DOWN){
								tram.x = segmentX-segmentLen/2;
							}else{
								tram.x = segmentX+segmentLen/2;
							}
							tram.y = segmentY;
						}else{
							tram.x = segmentX + segmentShape.data.tramPos.x;
							tram.y = segmentY + segmentShape.data.tramPos.y;
						}
						if(this.tramMap.containsKey(tram.tramCode) && !tram.end){ //存在，更新列车数据
							var tramShape = this.tramMap.get(tram.tramCode).refresh(tram);
						}else if(this.tramMap.containsKey(tram.tramCode) && tram.end){//车辆运营结束
							this.tramMap.get(tram.tramCode).shapeGroup.destroy();
							this.tramMap.remove(tram.tramCode);
						}else{	//不存在，新增列车数据
							var tramShape = new shape.Tram(tram , this.layer);
							tramShape.draw();
							this.tramMap.put(tram.tramCode , tramShape);
							(function(tram){
								tramShape.click(function(){
									if ($this.selectedTramCallback) {
										$this.selectedTramCallback(tram);
									}
								});
							})(tram);
						}
					}
				}
			}catch(e){
				console.log(e);
			}
		}
		this.layer.batchDraw();

	};
	/**
	 * 更新设备状态
	 * @param datas
	 */
	Linear.prototype.deviceChange = function(datas){

		for(var i = 0 ; i < datas.length ;i++){
			var data = datas[i];
			var shape = this.deviceMap.get(data.code);	
				
			if(shape!=null){
				if(data.status!=null && shape.updateStatus){
					shape.updateStatus(data.status,data.code);
				}
				if(data.opStatus && shape.updateOpStatus) {
					shape.updateOpStatus(data.opStatus);
				}
			}		
		}
		this.layer.batchDraw();
	};
	/**
	 * 全部设备离线
	 */
	Linear.prototype.deviceOffChange = function(){
		var $this = this;
		_.each($this.deviceMap.values(),function(shape){
			if(shape && shape.updateOnlineStatus){
				shape.updateOnlineStatus(false);
			}
		});
		this.layer.batchDraw();
	}
	Linear.prototype.selectOneRoute = function(routeItems){
		var $this = this;
		this.tempLayer.destroyChildren();
		_.each($this.timeoutArr ,function(timeout){
			clearInterval(timeout);
		});
		this.timeoutArr = [];
		for(var i = 0 ; i < routeItems.length ; i++){
			var routeItem = routeItems[i];
			var itemShape = this.deviceMap.get(routeItem.itemCode);
			if(itemShape instanceof shape.Segment){
				var timeout = itemShape.selected(this.tempLayer);
				this.timeoutArr.push(timeout);
			}else if(itemShape instanceof shape.Turnout){
				var timeout =itemShape.selected(this.tempLayer , routeItem.needStatus);
				this.timeoutArr.push(timeout);
			}
		}
		this.tempLayer.batchDraw();
	};
	Linear.prototype.cancelSelectedRoute = function(){
		var $this = this;
		this.tempLayer.destroyChildren();
		this.tempLayer.batchDraw();
		_.each($this.timeoutArr ,function(timeout){
			clearInterval(timeout);
		});
	};
	exports.Linear = Linear;
});