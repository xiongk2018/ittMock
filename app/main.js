﻿/**
	Smartram main js
*/
'use strict';
define(function(require , exports , module){
	require("bootstrap");
	require("angular");
	require("angular-route");
	require("angular-sanitize");
	require("bindonce");
	require("angular-lazyload");
	require("./common/main");
	require("underscore");
	
	var app = angular.module("app",['angular-lazyload', "pasvaz.bindonce" , 'ngRoute','common'])
	.config(["$routeProvider",function($routeProvider){
     //全景图（查看报警时找不到ctrl)
		$routeProvider.when("/linearDispatchMonitor" , {
			templateUrl:"./app/monitor/tpl/linearDispatch.html",
			controllerUrl: "./app/monitor/controllers/linearDispatchCtrl",
			controller:"linearDispatchCtrl"
		});
	}])
	.run(["$timeout" , "$rootScope" , function($timeout , $rootScope){
		$timeout(function(){
			$("#progressModal").hide();
			$("#progress").hide();
			window.location.href = "index.html#/linearDispatchMonitor";
		} , 1000);
	}]);
	//运行期
  	app.run(['$lazyload', function($lazyload){
    	//Step5: init lazyload & hold refs
    	$lazyload.init(app);
    	app.register = $lazyload.register;
    }
  	]);
	//module.exports = app;
	//window.app = app;
	angular.element(document).ready(function(){
        angular.bootstrap(document , ["app"]);
    });
	
});